<html>
    <head>
        <title>CIS 322 REST-api demo: Brevet</title>
    </head>

    <body>
        <h2>List All</h2>
        <ul>

          <?php
          $csv = file_get_contents('http://brevet:5000/listAll/csv');
          $data = str_getcsv($csv);
          $char = ":";
          $counter = 0;
          foreach ($data as $key => $val) {
            if(strpos($val, $char) !== false){
              echo "$val | ";
              $counter = $counter + 1;
            }
            if($counter === 2){
              echo "<br/>";
              $counter = 0;
            }
            else{
              echo "$val | ";
            }
          }
          ?>
        </ul>

        <h2>List Open</h2>
        <ul>

          <?php
          $csv = file_get_contents('http://brevet:5000/listOpenOnly/csv');
          $data = str_getcsv($csv);
          $char = ":";
          //var_dump($data);
          foreach ($data as $key => $val) {
            if(strpos($val, $char) !== false){
              echo "$val | ";
              echo "<br/>";
            }
            else{
              echo "$val | ";
            }
          }
          ?>
        </ul>

        <h2>List Open Top 3</h2>
        <ul>

          <?php
          $csv = file_get_contents('http://brevet:5000/listOpenOnly/csv?top=3');
          $data = str_getcsv($csv);
          $char = ":";
          //var_dump($data);
          foreach ($data as $key => $val) {
            if(strpos($val, $char) !== false){
              echo "$val | ";
              echo "<br/>";
            }
            else{
              echo "$val | ";
            }
          }
          ?>
        </ul>

        <h2>List Close</h2>
        <ul>

          <?php
          $csv = file_get_contents('http://brevet:5000/listCloseOnly/csv');
          $data = str_getcsv($csv);
          $char = ":";
          //var_dump($data);
          foreach ($data as $key => $val) {
            if(strpos($val, $char) !== false){
              echo "$val | ";
              echo "<br/>";
            }
            else{
              echo "$val | ";
            }
          }
          ?>
        </ul>

        <h2>List Close Top 3</h2>
        <ul>

          <?php
          $csv = file_get_contents('http://brevet:5000/listCloseOnly/csv?top=3');
          $data = str_getcsv($csv);
          $char = ":";
          //var_dump($data);
          foreach ($data as $key => $val) {
            if(strpos($val, $char) !== false){
              echo "$val | ";
              echo "<br/>";
            }
            else{
              echo "$val | ";
            }
          }
          ?>
        </ul>


    </body>
</html>
