"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import json
import logging
import os
import csv
from flask_restful import Resource, Api
from bson import json_util


###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("request.args: {}".format(request.args))

    length = request.args.get('endkm', 400, type=int)
    start_str = request.args.get('time_str', 'Sadge', type=str)
    date_time_arrow = arrow.get(start_str, 'YYYY-MM-DD HH:mm:ss')
    open_time = acp_times.open_time(km, length, date_time_arrow.isoformat())
    close_time = acp_times.close_time(km, length, date_time_arrow.isoformat())

    result = {"open": open_time, "close": close_time, "error": True}
    return flask.jsonify(result=result)

@app.route('/_submit')
def _submit():
    submit = json.loads(request.args.get('submit', type=str))
    db.tododb.remove({})
    db.tododb.insert_one(submit)
    return flask.render_template('calc.html')

@app.route('/_load')
def display():

    dict = db.tododb.find_one()
    x = []
    y = []

    for key in dict:
        x.append(key)
        y.append(dict[key])
    distlist = x[1:]
    timelist = y[1:]
    return flask.render_template('display.html', timelist=timelist, distlist=distlist)

class listAll(Resource):
    def get(self, type="json"):
        output = json_util.dumps(db.tododb.find({},{'_id':0}))
        if type == "csv":
            output = str(output)
            app.logger.info(output)
            output = output.replace("},", ", \n ")
            output = output.replace("}", "")
            output = output.replace("{", "")
            output = output.replace("]", "")
            output = output.replace("[[", "")
            output = output.replace('",', ", ")
            output = output.replace("[", "")
            output = output.replace(": ", ", ")
            output = output.replace('"', "")
            return flask.make_response(output)
        else:
            return flask.make_response(output)

class listClose(Resource):
    def get(self, type="json"):
        dict = db.tododb.find_one()
        x = []
        output = []
        for key in dict:
            x.append(key)
        list = x[1:]
        if request.args.get('top') is not None:
            top = int(request.args.get('top')) + 1
            list = x[1:top]
            app.logger.info(top)
        for item in list:
            helper = str(item) + ".close"
            output.append(db.tododb.find({},{helper:1, '_id':0}))
        output = json_util.dumps(output)
        if type == "csv":
            output = str(output)
            output = output.replace("}", "")
            output = output.replace("{", "")
            output = output.replace("]", "")
            output = output.replace("[[", "")
            output = output.replace(",", "")
            output = output.replace(" [", ", \n ")
            output = output.replace(": ", ", ")
            output = output.replace('"', "")
            return flask.make_response(output)
        else:
            return flask.make_response(output)

class listOpen(Resource):
    def get(self, type="json"):
        dict = db.tododb.find_one()
        x = []
        output = []
        for key in dict:
            x.append(key)
        list = x[1:]
        if request.args.get('top') is not None:
            top = int(request.args.get('top')) + 1
            #x = x[1:]
            #list = x[-(top-1):] #this is for last n
            list = x[1:top]
            app.logger.info(top)
        for item in list:
            helper = str(item) + ".open"
            output.append(db.tododb.find({},{helper:1, '_id':0}))
        output = json_util.dumps(output)
        if type == "csv":
            output = str(output)
            output = output.replace("}", "")
            output = output.replace("{", "")
            output = output.replace("]", "")
            output = output.replace("[[", "")
            output = output.replace(",", "")
            output = output.replace(" [", ', \n ')
            output = output.replace(": ", ", ")
            output = output.replace('"', "")
            return flask.make_response(output)
        else:
            return flask.make_response(output)


api.add_resource(listAll, '/listAll')
api.add_resource(listAll, '/listAll/<type>', endpoint="listAll")
api.add_resource(listClose, '/listCloseOnly')
api.add_resource(listClose, '/listCloseOnly/<type>', endpoint="listCloseOnly")
api.add_resource(listOpen, '/listOpenOnly')
api.add_resource(listOpen, '/listOpenOnly/<type>', endpoint="listOpenOnly")



#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
